# Vertical Form Helper (vertformhelper)

*Rails makes assumptions, so should forms.  This gem handles the vertical aspect of your form.  You can use as many vertical parts as you would like.  Make 3 side by side if you'd like.  The up and down will be aligned.*

Author:: Daniel P. Clark  [6ftdan@gmail.com](mailto:6ftdan@gmail.com)

License:: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](http://creativecommons.org/licenses/by-nc-nd/4.0/)

Give it a JSON file of `{"lable" => "rails code"}` and it builds a vertical table with it.  Now you can use your favorite format JSON for easy form management.

### Include `vertformhelper` in Your Gemfile

    gem "vertformhelper"


### In Your Form Add a JSON Formatted File This Way

    <%= form_for(@example) do |f| %>
      <%= vert_form Rails.root + "app/views/example/example.json", binding %>
    <% end %>

### Example JSON File

    {
      "Address:":     "f.select :kind, Address.kinds.keys, :default => :work",
      "Address 1:":   "f.text_field :address1",
      "Address 2:":   "f.text_field :address2",
      "Address 3:":   "f.text_field :address3",
      "City:":        "f.text_field :city",
      "State:":       "f.text_field :state",
      "Postal Code:": "f.text_field :postal_code",
      "Country:":     "f.country_select :country, ['United States of America'], { include_blank: true }, {class: 'chosen-select'}"
    }

### Example As Shown Below

![Example](https://bytebucket.org/danielpclark/vertformhelper/raw/8b208d5749d8aeb5da600c79d007d395ec64c581/form-example.png)