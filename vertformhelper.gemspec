$:.push File.expand_path("../lib", __FILE__)
require "vertformhelper/version"

Gem::Specification.new do |s|
  s.name        = "vertformhelper"
  s.version     = VertFormHelper::VERSION
  s.licenses    = ['Attribution-NonCommercial-NoDerivatives 4.0 International']
  s.authors     = ["Daniel P. Clark / 6ftDan(TM)"]
  s.summary     = "Rails vertical form generator from JSON."
  s.description = 'Rails vertical form generator from JSON. {"label" => "rails code"} '
  s.email       = 'webmaster@6ftdan.com'
  s.files       = [ "lib/vertformhelper.rb", "lib/vertformhelper/version.rb","lib/vertformhelper/railtie.rb", "lib/vertformhelper/view_helpers.rb" ]
  s.homepage    = 'http://bitbucket.org/danielpclark/vertformhelper'
  s.platform    = 'ruby'
  s.require_paths = ["lib"]
  s.add_runtime_dependency( 'vertform', '~> 0.0', '>= 0.0.2' )
  s.required_ruby_version = '>= 1.9.3'
end

