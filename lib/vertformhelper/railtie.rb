require 'vertformhelper/view_helpers'

module VertFormHelper
  class Railtie < Rails::Railtie
    initializer "vertformhelper.view_helpers" do
      ActionView::Base.send :include, ViewHelpers
    end
  end 
end
