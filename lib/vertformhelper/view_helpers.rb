require 'vertform'

module VertFormHelper
  module ViewHelpers
    def vert_form(file, binding)
      ERB.new(
        VertForm.new(
            JSON.parse( 
              IO.read(
                File.join(
                  file
                )
              )
            )
          ) .
        rend_form
      ) . 
      result(binding) . 
      html_safe
    end

    def vert_form_json(json, binding)
      ERB.new(
        VertForm.new(
            JSON.parse( 
              json
            )
          ) .
        rend_form
      ) . 
      result(binding) . 
      html_safe
    end
  end
end
